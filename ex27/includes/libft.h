# ifndef LIBFT
# define LIBFT

# define BUF_SIZE 164 
void    ft_putstr(char*);
int     ft_strcmp(char*, char*);
int     ft_strlen(char*);
void    ft_swap(int, int);

# endif 
