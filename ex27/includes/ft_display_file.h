/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/07 22:05:35 by tony              #+#    #+#             */
/*   Updated: 2019/06/07 22:06:03 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# ifndef FT_DISPLAY_FILE
# define FT_DISPLAY_FILE

#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

# endif
