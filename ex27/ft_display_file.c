/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/07 21:45:13 by tony              #+#    #+#             */
/*   Updated: 2019/06/07 22:25:52 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_display_file.h"

void    ft_display_file(char *filename)
{
    const int   fd = open(filename, O_RDONLY);
    char        buffer[BUF_SIZE+1];
    int         ret;

    if (fd != -1)
    {
        while ((ret = read(fd, buffer, BUF_SIZE)))
        {
            buffer[ret] = '\0';
            ft_putstr(buffer);
        }
        close(fd);
    }
}

int     main(int argc, char **argv)
{
    if (argc == 2)
        ft_display_file(argv[1]);
    else if (argc <= 2)
        ft_putstr("File name missing.\n");
    else
        ft_putstr("Too many arguments.\n");
    return (0);
}
