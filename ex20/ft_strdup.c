/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <tony@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/06 22:33:21 by tony              #+#    #+#             */
/*   Updated: 2019/06/07 18:48:23 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int     ft_strlen(char *str)
{
    const char *final_str = str;

    while (*str)
        str++;
    return (str - final_str);
}

char    *ft_strdup(char *src)
{
    const int   len = ft_strlen(src);
    char        *new;
    
    new = (char*)malloc(sizeof(char) * len + 1);
    if (new == NULL)
        return (NULL);
    while (*src)
        *new++ = *src++;
    return (new - len);
}
