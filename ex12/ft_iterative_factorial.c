/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: toto <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/06 00:49:14 by toto              #+#    #+#             */
/*   Updated: 2019/06/06 16:12:07 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int     ft_iterative_factorial(int nb)
{
   int i;
   int tmp;

   i = 0;
   tmp = 1;
   while (i++ < nb - 1)
       tmp = tmp * (i + 1);
   return ((nb <= 0 || nb > 16) ? 0 : tmp);
}
