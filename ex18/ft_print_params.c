/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/06 17:51:00 by tony              #+#    #+#             */
/*   Updated: 2019/06/06 17:57:14 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void    ft_putstr(char *str)
{
    const char *s = str;

    while (*str)
        str++;
    write(1, s, str - s);
}

int     main(int argc, char **argv)
{
    if (argc > 1)
        while(argc-- != 1)
        {
            ft_putstr(*++argv);
            write(1, "\n", 1);
        }
    return (0);
}
