/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/07 16:57:35 by tony              #+#    #+#             */
/*   Updated: 2019/06/07 17:12:07 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int     *ft_range(int min, int max)
{
    int *array;
    int total_size;

    if (min >= max)
        return (NULL);

    total_size = max - min;
    array = (int*)malloc(total_size * sizeof(int));
    if (array == NULL)
        return (NULL);
    while (min < max)
        *array++ = min++;
    return (array - total_size);
}
