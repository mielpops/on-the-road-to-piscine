/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/06 18:15:06 by tony              #+#    #+#             */
/*   Updated: 2019/06/06 21:59:56 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int     ft_strcmp(char *s1, char *s2)
{
    while (*s1 == *s2 && *s1++ && *s2++);
    return (*s1 - *s2);
}

void  ft_swap(char **s1, char **s2)
{
    char *tmp;
    
    tmp = *s1;
    *s1 = *s2;
    *s2 = tmp;
}

void    ft_putstr(char *str)
{
    const char *s = str;

    while (*str)
        str++;
    write(1, s, str - s);
}

void    ft_sort_string_array(char **array)
{
    const char  **final_array    = (const char**)array;

    while (*(array + 1))
    {
        if (ft_strcmp(*array, *(array+1)) > 0)
        {
            ft_swap(&(*array), &(*(array+1)));
            array = (char**)final_array;
        }
        else
            array++;
    }
}

int     main(int argc, char **argv)
{
    if (argc > 1)
    {
        ft_sort_string_array(++argv);
        while(argc-- != 1)
        {
            ft_putstr(*argv++);
            write(1, "\n", 1);
        }
    }
    return (0);
}
